# frozen_string_literal: true

Encoding.default_external = 'UTF-8'

require './lib/gitlab_api'

# Helper class to acquire MR/issues related information through the GitLab API
class Report
  attr_reader :token

  def initialize(token)
    @token = token
    # 9970 is the group id of gitlab-org https://gitlab.com/gitlab-org
    @endpoint_prefix = 'groups/9970'
  end

  def get_past_week_issues(labels, since, state = 'closed')
    params = {
      state: state,
      labels: labels,
      order_by: 'milestone_due'
    }
    issue_req = "#{@endpoint_prefix}/issues?#{Utils.compute_http_params(params)}"
    call_api(issue_req, since).select { |issue| issue['closed_at'] && DateTime.parse(issue['closed_at']) >= since }
  end

  def get_past_week_mrs(labels, since, state = 'merged')
    params = {
      state: state,
      labels: labels,
      order_by: 'updated_at'
    }
    mr_req = "#{@endpoint_prefix}/merge_requests?#{Utils.compute_http_params(params)}"
    call_api(mr_req, since).select { |mr| mr['merged_at'] && DateTime.parse(mr['merged_at']) >= since }
  end

  def call_api(req, updated_after)
    api.get(req, updated_after, auth: true).uniq
  end

  def api
    @api ||= GitlabApi.new(token: token)
  end
end
