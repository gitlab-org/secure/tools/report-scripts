#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rspec'
require_relative '../lib/util'

describe Utils do
  context 'utilities' do
    it 'should escape markdown links' do
      expect(Utils.markdown_link('name````', 'http://test') == '[name](http://test)')
      expect(Utils.markdown_link('test````<>""""', 'http://test') == '[test](http://test)')
    end

    it 'should generate proper group labels' do
      expect(Utils.group_label('test') == 'group::test')
    end

    it 'should compute proper http params' do
      expect(Utils.compute_http_params('a': 'b', 'c': 'd') == 'a=b&c=d')
    end

    context 'group_selectors_by' do
      it 'selects correct labels for secure group' do
        expect(
          Utils.group_selectors_by(teams: ['Static Analysis'])
        ).to eq('devops::secure,group::static%20analysis' => 'Static Analysis')
      end

      it 'selects correct labels for protect group' do
        expect(
          Utils.group_selectors_by(teams: ['Container Security'])
        ).to eq('devops::protect,group::container%20security' => 'Container Security')
      end

      it 'appends relevant labels' do
        expect(
          Utils.group_selectors_by(teams: ['Static Analysis'], labels: 'feature')
        ).to eq('devops::secure,group::static%20analysis,feature' => 'Static Analysis')
      end
    end
  end
end
